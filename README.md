# Workload Identity Federation for GitLab

Example `gitlab-ci.yml` for Google Cloud Workload Identity Federation.

For more help and how-to please see: <https://github.com/Cyclenerd/google-workload-identity-federation>